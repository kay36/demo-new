package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.HelpDeskModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HelpDeskRepository extends MongoRepository<HelpDeskModel, String> {
}
