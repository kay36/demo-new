package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.PurchaseBidsModel;
import com.vinbytes.binary.model.TransactionModal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends MongoRepository<TransactionModal, String> {

    Page<TransactionModal> findAllByUserIdOrderByTransactionInitiateDateAndTimeDesc(String userId, Pageable pageable);

    Page<TransactionModal> findAllByUserId(String userId,Pageable pageable);

    List<TransactionModal> findAllByUserId(String userId);

    List<TransactionModal> findAllByUserIdAndTransactionId(String userId, String transactionId);

    Page<TransactionModal> findAll(Pageable pageable);

    TransactionModal findByTransactionId(String transactionId);

    Page<TransactionModal> findAllByUserIdAndWalletTypeOrderByTransactionInitiateDateAndTimeDesc(String userId, String walletType, Pageable pageable);

    Page<TransactionModal> findAllByUserIdAndTransactionStatusOrderByTransactionInitiateDateAndTimeDesc(String userId,String transactionStatus, Pageable pageable);

    TransactionModal findByCoinPaymentsDepositResponseTxnId(String txn_id);
    TransactionModal findByCoinPaymentsWithdrawalResponseId(String id);
    List<TransactionModal> findTop3ByTransactionTypeOrderByBidAmountDesc(String transactionType);
}
