package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.Twilio2FAModal;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Twilio2FARepository extends MongoRepository<Twilio2FAModal, String> {

    Twilio2FAModal findByUserIdAndTwilioId(String userId, String twilioId);
}
