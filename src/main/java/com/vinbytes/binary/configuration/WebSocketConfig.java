package com.vinbytes.binary.configuration;

import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.UserRepository;
import com.vinbytes.binary.services.CustomHandshakeHandler;
import com.vinbytes.binary.services.KeyCloakService;
import com.vinbytes.binary.services.StompPrincipal;
import com.vinbytes.binary.services.UserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;


@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Autowired
    public KeyCloakService keyCloakService;

    @Autowired
    private UserRepository userRepository;

    /*@Autowired
    private JwtDecoder jwtDecoder;*/


    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }



    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").setAllowedOriginPatterns("*").setHandshakeHandler(new CustomHandshakeHandler()).withSockJS();
    }

/*    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(new UserInterceptor());
    }*/




    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
//        registration.interceptors(new UserInterceptor());
        registration.interceptors(new ChannelInterceptorAdapter() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                    Optional.ofNullable(accessor.getNativeHeader("Authorization")).ifPresent(ah -> {
                        String bearerToken = ah.get(0).replace("Bearer ", "");
                        /*System.out.println(bearerToken);
                        Jwt jwt = jwtDecoder.decode(bearerToken);
                        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
                        Authentication authentication = converter.convert(jwt);
                        accessor.setUser(authentication);*/
                    });
                }
                return message;
            }
        });
    }
}


