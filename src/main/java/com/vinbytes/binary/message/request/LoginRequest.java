
package com.vinbytes.binary.message.request;

import java.io.Serializable;
import java.util.Set;

public class LoginRequest implements Serializable {

    private static final long serialVersionUID = 774361628448131285L;


    //@NotBlank
    private String phoneNoOrEmail;

    private String countryCode;

    //@NotBlank
    private String password;

    private String loginType;

    private String roles;

    private String username;

    private String email;

    private String googleUserId;

    private String appleUserId;

    private String profileUrl;

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNoOrEmail() {
        return phoneNoOrEmail;
    }

    public void setPhoneNoOrEmail(String phoneNoOrEmail) {
        this.phoneNoOrEmail = phoneNoOrEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGoogleUserId() {
        return googleUserId;
    }

    public void setGoogleUserId(String googleUserId) {
        this.googleUserId = googleUserId;
    }

    public String getAppleUserId() {
        return appleUserId;
    }

    public void setAppleUserId(String appleUserId) {
        this.appleUserId = appleUserId;
    }
}
