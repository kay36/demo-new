package com.vinbytes.binary.services;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.vinbytes.binary.model.UserModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private String id;

	private String username;

	private String email;

	@JsonIgnore
	private String password;

	private String countryCode;

	private String phoneNo;

	private String profileUrl;

	private String otp;

	private boolean verified;

	private boolean resetPassword;

	private String loginType;

	private String roles;

	private boolean deleted;

	private String authorities;

	public UserDetailsImpl(String id, String username, String email, String roles, String password, String countryCode, String phoneNo, String profileUrl,
						   String otp, boolean verified, boolean resetPassword, String loginType, boolean deleted) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.password = password;
		this.countryCode = countryCode;
		this.phoneNo = phoneNo;
		this.profileUrl = profileUrl;
		this.otp = otp;
		this.verified = verified;
		this.resetPassword = resetPassword;
		this.loginType = loginType;
		this.deleted=deleted;
//		this.authorities = authorities;
	}

	public static UserDetailsImpl build(UserModel user) {
		String authorities = user.getRoles();

		return new UserDetailsImpl(user.getId(), user.getUsername(),user.getEmail(), user.getRoles(),user.getPassword(),user.getCountryCode(), user.getPhoneNo(),user.getProfileUrl(), user.getOtp(), user.isVerified(),user.isResetPassword(),user.getLoginType(),user.isDeleted());
	}

//	@Override
//	public String getAuthorities() {
//		return authorities;
//	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(id, user.id);
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public boolean isResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(boolean resetPassword) {
		this.resetPassword = resetPassword;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}
}
