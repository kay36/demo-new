package com.vinbytes.binary.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.vinbytes.binary.annotation.RequiresCaptcha;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.*;
import com.vinbytes.binary.services.*;
import io.swagger.annotations.*;
import org.bson.types.ObjectId;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.json.JSONObject;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.representations.AccessTokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.*;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.vinbytes.binary.security.JwtUtils;
import com.vinbytes.binary.message.request.LoginRequest;
import com.vinbytes.binary.message.request.SignupRequest;
import com.vinbytes.binary.message.response.JwtResponse;
import com.vinbytes.binary.message.response.MessageResponse;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

@Api(tags = "Authentication Controller")

@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)

@RestController
@RequestMapping("/api/auth")
public class AuthController extends  Twilio2FAService {

	private static final Logger log = LoggerFactory.getLogger(AuthController.class);


	@Value("${binaryOptions.app.mailExpiration}")
	private int mailExpirationTime;

	@Value("${file.upload-dir}")
	private String FileUploadMainDir;

	/*@Autowired
	private AuthenticationManager authenticationManager;*/

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private NotifyMeRepository notifyMeRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private EmailService emailService;

	@Autowired
	private VerifiedTokenRepository verifiedTokenRepository;

	@Autowired
	private PasswordResetTokenRepository passwordResetTokenRepository;

	@Autowired
	private WalletRepository walletRepository;

	@Autowired
	private JwtUtils jwtUtils;

	@Autowired
	private PermissionsRepository permissionsRepository;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	private OverallProgressRepository overallProgressRepository;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private KeyCloakService keyCloakService;

	@Autowired
	private walletServices walletServices;

	@Autowired
	private AmazonS3BucketService amazonS3BucketService;

	@Autowired
	private TwilioTwoFactorController twilioTwoFactorController;

	final String TestMoneyWalletURL = "/wallet/add/test/money/";

	final String SetInitPermissionsURL = "/permissions/set/init/";

	String baseUrl = "https://sandbox.play-crypto.com";
//	String baseUrl = "http://localhost:4200";
	RestTemplate restTemplate = new RestTemplate();

	@RequestMapping(value = "/**", method = RequestMethod.OPTIONS)
	public void corsHeaders(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
		response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with");
		response.addHeader("Access-Control-Max-Age", "3600");
	}

	@PostMapping("/keycloak/verify/{code}")
	public ResponseEntity<Object> authenticateUserFromKeycloakUsingCode(@PathVariable("code") String code) throws Exception {
		try {
			AccessTokenResponse response = keyCloakService.TokenExchange(code);
			if (response.getToken()!=null){
				String token = response.getToken();
				Base64.Decoder decoder = Base64.getUrlDecoder();
				String[] chunks = token.split("\\.");
				String header = new String(decoder.decode(chunks[0]));
				String payload = new String(decoder.decode(chunks[1]));
				JSONObject jsonObject = new JSONObject(payload);
				String email = (String) jsonObject.get("email");
				UserModel userModelFromDB = userRepository.findByEmail(email);

				if (userModelFromDB!=null){
					return ResponseEntity.ok().body(response);
				}else {
					return ResponseEntity.ok().body(payload);
				}
			}else {
				return ResponseEntity.badRequest().body(new MessageResponse("Invalid Code"));
			}
		}catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info : " + e));
		}
	}

	@ApiOperation(value = "Sign In", notes = "POST API to Sign In.")
	@RequiresCaptcha
	@PostMapping("/signin")
	public ResponseEntity<Object> authenticateUser(@ApiParam(value = "LoginRequest Object", required = true) @Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request) throws Exception {
		try {
			String data = loginRequest.getPhoneNoOrEmail().toLowerCase();
			if (loginRequest.getLoginType().equals("normal")){
				if (userRepository.existsByEmail(loginRequest.getPhoneNoOrEmail()) == false && userRepository.existsByPhoneNo(loginRequest.getPhoneNoOrEmail()) == false) {

					if (data.matches("^[a-zA-Z]*$") == true) {
						return ResponseEntity.badRequest()
								.body(new MessageResponse("Incorrect Username or Password!"));
					} else if (data.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$") == true) {
						return ResponseEntity.badRequest()
								.body(new MessageResponse("This Email-ID is not registered with us."));
					} else if (data.matches("[0-9]+") == true) {
						return ResponseEntity.badRequest()
								.body(new MessageResponse("Incorrect Username or Password!"));
					}
				}
			}

			try {
				AccessTokenResponse response;
				if(loginRequest.getLoginType().equals("normal")){
					UserModel model1= userRepository.findByPhoneNoOrEmail(loginRequest.getPhoneNoOrEmail(),loginRequest.getPhoneNoOrEmail()).orElse(null);
					if (model1 == null){
						return ResponseEntity.badRequest()
								.body(new MessageResponse("Incorrect Username or Password!"));
					}else {

						if(model1 !=null && !model1.isVerified()){
							return ResponseEntity.badRequest()
									.body(new MessageResponse("Your Account is not verified yet, please verify"));
						}
						if(model1 !=null && model1.isBlocked()){
							return ResponseEntity.badRequest()
									.body(new MessageResponse("Your Account is  Blocked .Please Contact our Help Desk."));
						}
					/*if(model1.isLoggedIn())
					{
						return ResponseEntity.badRequest()
								.body(new MessageResponse("Your last session is currently active. "));
					}
					model1.setLoggedIn(true);
					userRepository.save(model1);*/
					/*Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getPhoneNoOrEmail(), loginRequest.getPassword()));
					SecurityContextHolder.getContext().setAuthentication(authentication);
					String jwt = jwtUtils.generateJwtToken(authentication);*/
						boolean isPasswordMatch = encoder.matches(model1.getPassword(), loginRequest.getPassword());
						if (isPasswordMatch){
							response = keyCloakService.userLogin(loginRequest.getPhoneNoOrEmail(), model1.getPhoneNo());
							if (response.getToken()==null){
								return ResponseEntity.badRequest()
										.body(new MessageResponse("Incorrect Username or Password!"));
							}else {
								if(model1.isTwoFactorFlag()){
									boolean twilioMsgResponse = Twilio2FA(model1, "phone");
									if(twilioMsgResponse){
										return ResponseEntity.ok().body(new MessageResponse("OTP sent successfully"));
									}else{
										return ResponseEntity.badRequest().body(new MessageResponse("Error info "));
									}
								}else {
									model1.setLastLoginDate(new Date());
									if(model1.getLoggedCount()==0)
									{
										model1.setLoggedCount(1);
									}
									else
									{
										model1.setLoggedCount(model1.getLoggedCount()+1);
									}

									userRepository.save(model1);

									/*UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();*/
									return ResponseEntity.ok(new JwtResponse(response,model1.getId(), model1.getUsername(),
											model1.getEmail(), model1.getRoles(), model1.getCountryCode(), model1.getPhoneNo(),
											model1.getProfileUrl(), model1.getOtp(), model1.isVerified(),
											model1.isResetPassword(), model1.getLoginType(), model1.isDeleted(), model1.getCryptoAddress()));
								}
							}
						}else {
							return ResponseEntity.badRequest()
									.body(new MessageResponse("Incorrect Username or Password!"));
						}
					}
				}

				else if (loginRequest.getLoginType().equals("google") || loginRequest.getLoginType().equals("apple")) {
					if(userRepository.existsByEmail(loginRequest.getEmail())){
						UserModel user = userRepository.findByEmail(loginRequest.getEmail());
						response = keyCloakService.userLogin(user.getEmail(), user.getPhoneNo());
						if (response.getToken()==null){
							return ResponseEntity.badRequest()
									.body(new MessageResponse("Incorrect Username or Password!"));
						}else {
							String image = loginRequest.getProfileUrl();
							if (image != null) {
								String userProfileImageFile = null;
								String userProfileImageFileExt = null;
								String userProfileImageUrl = null;
								UtilBase64Image uts = new UtilBase64Image();
								String regex = "data:image/([a-zA-Z]*);base64,([^\"]*)";
								if (image.matches(regex)) {
									userProfileImageFile = uts.Base64String(image);
									userProfileImageFileExt = uts.Base64FileExtenstion(image);
									long common = new Date().getTime();

									byte[] bI = org.apache.commons.codec.binary.Base64.decodeBase64((image.substring(image.indexOf(",")+1)).getBytes());
									InputStream targetImageStream = new ByteArrayInputStream(bI);

									//set file metadata
									ObjectMetadata metadata = new ObjectMetadata();
									metadata.setContentLength(bI.length);
									metadata.setContentType(userProfileImageFileExt);
									metadata.setCacheControl("public, max-age=31536000");
									amazonS3BucketService.upload("play-crypto-dev/users",user.getUsername() + common + "profileImageUrl"+"."+userProfileImageFileExt, targetImageStream, metadata);
									user.setProfileUrl(user.getUsername() + common + "profileImageUrl"+"."+userProfileImageFileExt);

								} else {
									user.setProfileUrl(user.getProfileUrl());
								}
							}
							user.setLastLoginDate(new Date());
							user.setUsername(loginRequest.getUsername());
							if (!user.getTypesOfLogins().contains(loginRequest.getLoginType())){
								List<String> userLoginTypes = user.getTypesOfLogins();
								userLoginTypes.add(loginRequest.getLoginType());
								user.setTypesOfLogins(userLoginTypes);
							}
							userRepository.save(user);
							return ResponseEntity.ok(new JwtResponse(response, user.getId(), user.getUsername(),
									user.getEmail(), user.getRoles(), user.getCountryCode(),
									user.getPhoneNo(), user.getProfileUrl(), user.getOtp(),
									user.isVerified(), user.isResetPassword(), user.getLoginType(), user.isDeleted(),user.getCryptoAddress()));
						}
					}else {

//						Response SignupResponse = keyCloakService.userSignup(loginRequest.getUsername(), loginRequest.getPhoneNoOrEmail(), loginRequest.getEmail(), loginRequest.getPhoneNoOrEmail());
//						AccessTokenResponse SignupResponse = keyCloakService.userLogin(loginRequest.getPhoneNoOrEmail(), loginRequest.getPhoneNoOrEmail());
						boolean updateKeycloakUser = keyCloakService.userUpdatePhone(loginRequest.getEmail(), loginRequest.getPhoneNoOrEmail(), loginRequest.getPhoneNoOrEmail());
						if (updateKeycloakUser){
							return ResponseEntity.badRequest()
									.body(new MessageResponse("Unable to signup!"));
						}else {
							String loginUserId;
							if(loginRequest.getLoginType().equals("google")){
								loginUserId = loginRequest.getGoogleUserId();
							}else if(loginRequest.getLoginType().equals("apple")){
								loginUserId = loginRequest.getAppleUserId();
							}else {
								return ResponseEntity.badRequest()
										.body(new MessageResponse("Something went wrong while signin - authenticateUser()"));
							}
							List<String> typesOfLogins = new ArrayList<>();
							typesOfLogins.add(loginRequest.getLoginType());

							UserModel user = new UserModel(loginRequest.getLoginType(), loginRequest.getUsername(), loginRequest.getEmail(), loginUserId, loginRequest.getRoles());
							user.setPassword(null);
							user.setPhoneNo(loginRequest.getPhoneNoOrEmail());
							user.setCountryCode(loginRequest.getCountryCode());
							user.setTypesOfLogins(typesOfLogins);
							String image = loginRequest.getProfileUrl();
							if (image != null) {
								String userProfileImageFile = null;
								String userProfileImageFileExt = null;
								String userProfileImageUrl = null;
								UtilBase64Image uts = new UtilBase64Image();
								String regex = "data:image/([a-zA-Z]*);base64,([^\"]*)";
								if (image.matches(regex)) {
									userProfileImageFile = uts.Base64String(image);
									userProfileImageFileExt = uts.Base64FileExtenstion(image);
									long common = new Date().getTime();

									byte[] bI = org.apache.commons.codec.binary.Base64.decodeBase64((image.substring(image.indexOf(",")+1)).getBytes());
									InputStream targetImageStream = new ByteArrayInputStream(bI);

									//set file metadata
									ObjectMetadata metadata = new ObjectMetadata();
									metadata.setContentLength(bI.length);
									metadata.setContentType(userProfileImageFileExt);
									metadata.setCacheControl("public, max-age=31536000");
									amazonS3BucketService.upload("play-crypto-dev/users",user.getUsername() + common + "profileImageUrl"+"."+userProfileImageFileExt, targetImageStream, metadata);
									user.setProfileUrl(user.getUsername() + common + "profileImageUrl"+"."+userProfileImageFileExt);

								} else {
									user.setProfileUrl(user.getProfileUrl());
								}
							}
							user.setLoginType(loginRequest.getLoginType());
							user.setUsername(loginRequest.getUsername());
							user.setEmail(loginRequest.getEmail());
							user.setGoogleUserId(loginRequest.getGoogleUserId());
							user.setAppleUserId(loginRequest.getAppleUserId());
							user.setRoles(loginRequest.getRoles());
							user.setLastLoginDate(new Date());
							user.setJoinedOnDate(new Date());
							user.setVerified(true);
							if(user.getLoggedCount()==0)
							{
								user.setLoggedCount(1);
							}
							else
							{
								user.setLoggedCount(user.getLoggedCount()+1);
							}


							userRepository.save(user);

					/*		emailService.sendMail(loginRequest.getUsername(), loginRequest.getEmail(), "Welcome To Play-Crypto",
									baseUrl);*/

							UserModel userFromDb = userRepository.findByEmail(loginRequest.getEmail());
							response = keyCloakService.userLogin(loginRequest.getPhoneNoOrEmail(), loginRequest.getPhoneNoOrEmail());

							boolean walletInitializeStatus = walletServices.initialWalletCreationForUser(userFromDb.getId());
							if (!walletInitializeStatus){
								return ResponseEntity.badRequest().body(new MessageResponse("user wallet Init failed !"));
							}

							boolean permissionInitStatus = setInitPermissions(userFromDb.getId());
							if (!permissionInitStatus){
								return ResponseEntity.badRequest().body(new MessageResponse("user permission Init failed !"));
							}
							return ResponseEntity.ok(new JwtResponse(response, userFromDb.getId(), userFromDb.getUsername(), userFromDb.getEmail(),
									userFromDb.getRoles(), userFromDb.getCountryCode(), userFromDb.getPhoneNo(), userFromDb.getProfileUrl(),
									userFromDb.getOtp(), userFromDb.isVerified(), userFromDb.isResetPassword(), userFromDb.getLoginType(), userFromDb.isDeleted(),userFromDb.getCryptoAddress()));
						}
					}
				}

			} catch (DisabledException e) {
				log.error("USER_DISABLED");
				return ResponseEntity.badRequest()
						.body(new MessageResponse("Error: User is disabled for " + loginRequest.getPhoneNoOrEmail()));
			} catch (BadCredentialsException e) {
				log.error("INVALID_CREDENTIALS");
				return ResponseEntity.badRequest()
						.body(new MessageResponse("Your Username or Password is incorrect!"));
			}
		} catch (NullPointerException e){
			log.error("NULL_POINTER");
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Error info : " + e));
		}

		return ResponseEntity.badRequest()
				.body(new MessageResponse("Something went wrong while signin - authenticateUser()"));
	}



	@ApiOperation(value = "Sign Up", notes = "POST API to Sign Up.")
	@RequiresCaptcha
	@PostMapping("/signup")
	public ResponseEntity<Object> registerUser(@ApiParam(value = "SignInRequest Object", required = true) @Valid @RequestBody SignupRequest signUpRequest, HttpServletRequest request) throws MessagingException {
		try {
			log.info("inside AuthController.registerUser() method");
			UserModel userTemp = userRepository.findByEmail(signUpRequest.getEmail());
			if (userRepository.existsByEmail(signUpRequest.getEmail()) && userTemp.isVerified() == false) {
				return ResponseEntity.badRequest().body(new MessageResponse("You have already register with us! Would you like to resend the Welcome Mail?"));
			} else {


			if (userRepository.existsByEmail(signUpRequest.getEmail())) {
				return ResponseEntity.badRequest().body(new MessageResponse("This email id is already registered!"));
			}
			if (userRepository.existsByPhoneNo(signUpRequest.getPhoneNo())) {
				return ResponseEntity.badRequest().body(new MessageResponse("This Phone Number is already registered!"));
			}
				/*Response response = keyCloakService.userSignup(signUpRequest.getUsername(), signUpRequest.getPhoneNo(), signUpRequest.getEmail(), signUpRequest.getPassword());*/
				Response response = keyCloakService.userSignup(signUpRequest.getUsername(), signUpRequest.getPhoneNo(), signUpRequest.getEmail(), signUpRequest.getPhoneNo());
			if (response.getStatus()==201){
				List<String> typesOfLogins = new ArrayList<>();
				typesOfLogins.add(signUpRequest.getLoginType());
				UserModel user = new UserModel(signUpRequest.getUsername(),
						signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()), signUpRequest.getCountryCode(), signUpRequest.getPhoneNo(),
						signUpRequest.getProfileUrl(), signUpRequest.getLoginType(), signUpRequest.isDeleted());
						/*long common = new Date().getTime();
						UtilBase64Image uts = new UtilBase64Image();
						String profileFile = uts.Base64String(profile);
						String profileExt = uts.Base64FileExtenstion(profile);
						String profileUrl = uts.decoder(profileFile, fileStorageProperties.getUploadDir() + "User/"
								+ signUpRequest.getUsername() + common + "ProfileImage" + "." + profileExt);
						byte[] imageByteArraystore = java.util.Base64.getDecoder().decode(profileFile);
						user.setProfileUrl(profileUrl);*/
						user.setRoles(signUpRequest.getRoles());
						user.setLoginType("normal");
						user.setCountryCode(signUpRequest.getCountryCode());
						user.setDeleted(false);
						user.setResetPassword(false);
						user.setVerified(false);
						user.setTypesOfLogins(typesOfLogins);
		//				user.setProfileImageUrl();
						user.setJoinedOnDate(new Date());
						user.setProfileUrl("CommonUserProfile.png");
						userRepository.save(user);
						VerifiedTokenModel token = new VerifiedTokenModel();
						token.setToken(encoder.encode(UUID.randomUUID().toString()));
						token.setUser(user);
						token.setExpiryDate(mailExpirationTime);
						verifiedTokenRepository.save(token);

						UserModel userFromDb = userRepository.findByEmail(signUpRequest.getEmail());
//						String appUrl = "http://play-crypto.com/verify?token=";

						String appUrl = "https://sandbox.play-crypto.com"+"/verify?token=";
						emailService.sendMail(user.getUsername(), user.getEmail(), "Verification Mail", appUrl + token.getToken());

						UserModel userTempWalletInit = userRepository.findByEmail(signUpRequest.getEmail());
						boolean walletInitializeStatus = walletServices.initialWalletCreationForUser(userTempWalletInit.getId());
						if (!walletInitializeStatus){
							return ResponseEntity.badRequest().body(new MessageResponse("user wallet Init failed !"));
						}

						/*String Base = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + request.getContextPath();
						System.out.println(restTemplate.getForObject(Base + TestMoneyWalletURL + userFromDb.getId(), String.class));
						System.out.println(restTemplate.getForObject(Base + SetInitPermissionsURL + userFromDb.getId(), String.class));*/

						boolean permissionInitStatus = setInitPermissions(userFromDb.getId());
						if (!permissionInitStatus){
							return ResponseEntity.badRequest().body(new MessageResponse("user permission Init failed !"));
						}

						return ResponseEntity.ok(new MessageResponse("Check Your Email ,Verify your email address to use your account" +
						"We sent an email to" + user.getUsername() + "with a button to verify your email address.!"));
			}else {
				return ResponseEntity.badRequest().body(new MessageResponse("Email Or Phone Number is already in use!"));
			}
		}
		}
		catch (Exception e){
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
		}
	}


	@PostMapping("/signin/{code}")
	public ResponseEntity<Object> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, @PathVariable("code") String code){
		try {
			UserModel userFrmDb= userRepository.findByPhoneNoOrEmail(loginRequest.getPhoneNoOrEmail(),loginRequest.getPhoneNoOrEmail()).orElse(null);
			AccessTokenResponse response = keyCloakService.userLogin(loginRequest.getPhoneNoOrEmail(), loginRequest.getPassword());
			if (userFrmDb == null){
				return ResponseEntity.badRequest()
						.body(new MessageResponse("Incorrect Username or Password!"));
			}else {
				if (response.getToken()==null){
					return ResponseEntity.badRequest()
							.body(new MessageResponse("Incorrect Username or Password!"));
				}else {
					boolean twilioOtpResponse = Twilio2FACodeCheck(userFrmDb, code, "phone");
					if(twilioOtpResponse){
						userFrmDb.setLastLoginDate(new Date());
						userRepository.save(userFrmDb);
						return ResponseEntity.ok(new JwtResponse(response,userFrmDb.getId(), userFrmDb.getUsername(),
								userFrmDb.getEmail(), userFrmDb.getRoles(), userFrmDb.getCountryCode(), userFrmDb.getPhoneNo(),
								userFrmDb.getProfileUrl(), userFrmDb.getOtp(), userFrmDb.isVerified(),
								userFrmDb.isResetPassword(), userFrmDb.getLoginType(), userFrmDb.isDeleted(), userFrmDb.getCryptoAddress()));
					}else{
						return ResponseEntity.badRequest().body(new MessageResponse("OTP is invalid or expired"));
					}
				}
			}
		} catch (Exception e) {
			log.error("NULL_POINTER");
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Error info : " + e));
		}
	}


	@PostMapping("/refresh-token")
	public ResponseEntity<Object> refreshTokenGenerate(@RequestBody String refreshToken){
		AccessTokenResponse Token = keyCloakService.userTokenRefresh(refreshToken);
		if (Token.getToken()==null){
			return ResponseEntity.badRequest()
					.body(new MessageResponse("Unauthorized Access !"));
		}else {
			return ResponseEntity.ok(Token);
		}
	}


	@PostMapping("/sent/email")
	public ResponseEntity<Object> sentcheck() throws Exception {
//	emailService.sendLostMail("Taamilselvan","b.tamilselvan13@gmail.com","Lost","Bitcoin","#5M12054","2","3 min");
	return ResponseEntity.ok("Done");
	}

	@PostMapping("/resend-mail")
	public ResponseEntity<?> resendWelcomeMAil(@RequestBody LoginRequest signUpRequest) throws Exception {

		UserModel userFromDb = userRepository.findByEmail(signUpRequest.getEmail());
		String verificationToken = UUID.randomUUID().toString();
		String appUrl = baseUrl + "/verify?token=";
		try{
			if(userFromDb==null){
				return ResponseEntity.badRequest().body("User not found");
			}
			else {
				System.out.println(userFromDb.getEmail()+".......");
				if (userRepository.existsByEmail(userFromDb.getEmail())) {
					VerifiedTokenModel token = new VerifiedTokenModel();
					token.setToken(verificationToken);
					token.setUser(userFromDb);
					token.setExpiryDate(mailExpirationTime);
					verifiedTokenRepository.save(token);

					emailService.sendMail(userFromDb.getUsername(), userFromDb.getEmail(), "Verification Mail", appUrl + verificationToken);
					return ResponseEntity.ok(new MessageResponse("Welcome mail resend successfully"));
				}else {
					return ResponseEntity.badRequest().body("User not found");
				}
			}
		}catch (Exception e)
		{
			System.out.println(e.getMessage());
			return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}


//	@ApiOperation(value = "Update Verified in Users by Id", notes = "PUT API to Update Verified in Users by Id")
//	@PutMapping(value = "/verified/{id}")
//	@ResponseStatus(value = HttpStatus.NO_CONTENT)
//	public ResponseEntity<Object> verifiedUsersById(
//			@ApiParam(value = "Users Id to update verified object", required = true) @PathVariable("id") ObjectId id,
//			@RequestParam("Verified") boolean isverified) {
//		log.info("inside AuthController.verifiedUsersById() method");
//
//		if (!userRepository.existsByIdAndDeleted(id, false)) {
//			return new ResponseEntity<Object>("UserId " + id + " cannot be updated", HttpStatus.BAD_REQUEST);
//		}
//
//		UserModel model2 = userRepository.findById(id);
//		model2.setVerified(isverified);
//		userRepository.save(model2);
//		return new ResponseEntity<Object>("UserId " + id + " SuccessFully Verified", HttpStatus.OK);
//	}


	/*May-28*/
	@ApiOperation(value = "Get User By token", notes = "Get User By token")
	@GetMapping(value = "/get/user")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> getUserByToken() {
		log.info("inside AuthController.getUserByToken() method");
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String userName = principal.toString();
			String userEmail = keyCloakService.getUserWithName(userName);
			if (!userEmail.equals("User Not Found !")) {
				UserModel user = userRepository.findByEmail(userEmail);
				if (user == null) {
					return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
				}else {
					return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "user SuccessFully found")
							.body(user);
				}
			}else {
				return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
			}

		}catch (Exception e){
			return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	@ApiOperation(value = "Update User Profile", notes = "Update User Profile")
	@PostMapping(value = "/update/user/profile")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> UpdateUserProfilePic(@ApiParam(value = "Update User Profile", required = true) @Valid @RequestBody UserModel userToUpdate) {
		log.info("inside AuthController.UpdateUserProfile() method");
		if (userToUpdate==null){
			return ResponseEntity.badRequest().body(new MessageResponse("Bad Request !"));
		}else {
			try {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				String userName = principal.toString();
				String userEmail = keyCloakService.getUserWithName(userName);
				if (!userEmail.equals("User Not Found !")) {
					UserModel user = userRepository.findByEmail(userEmail);
					if (user == null) {
						return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
					}else {
						String image = userToUpdate.getProfileUrl();
						if(image!=null && image.equals("remove"))
						{
							user.setProfileUrl("CommonUserProfile.png");
						}
						String username = userToUpdate.getUsername();
						if (username!=null){
							user.setUsername(userToUpdate.getUsername());
						}

						if (image!=null && !image.equals("remove")){
							String userProfileImageFile = null;
							String userProfileImageFileExt = null;
							String userProfileImageUrl = null;
							UtilBase64Image uts = new UtilBase64Image();
							String regex = "data:image/([a-zA-Z]*);base64,([^\"]*)";
							if (image.matches(regex)) {
								userProfileImageFile = uts.Base64String(image);
								userProfileImageFileExt = uts.Base64FileExtenstion(image);
								long common = new Date().getTime();

								byte[] bI = org.apache.commons.codec.binary.Base64.decodeBase64((image.substring(image.indexOf(",")+1)).getBytes());
								InputStream targetImageStream = new ByteArrayInputStream(bI);

								//set file metadata
								ObjectMetadata metadata = new ObjectMetadata();
								metadata.setContentLength(bI.length);
								metadata.setContentType(userProfileImageFileExt);
								metadata.setCacheControl("public, max-age=31536000");
								amazonS3BucketService.upload("play-crypto-dev/users",user.getUsername() + common + "profileImageUrl"+"."+userProfileImageFileExt, targetImageStream, metadata);
								user.setProfileUrl(user.getUsername() + common + "profileImageUrl"+"."+userProfileImageFileExt);

							} else {
								user.setProfileUrl(user.getProfileUrl());
							}
						}

						userRepository.save(user);

						/*boolean MakeNotificationForUser = notificationService.MakeNotifications(user.getId(), user.getUsername(), "profile  updated successfully", user.getProfileUrl(), "/settings");
						if (!MakeNotificationForUser){
							log.info("User Notification - unexpected Error !! - inside AuthController.UpdateUserProfile() method");
						}*/

						return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "user SuccessFully found")
								.body(user);
					}
				}else {
					return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
				}

			}catch (Exception e){
				return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@ApiOperation(value = "Get User By token", notes = "Get User By token")
	@PostMapping(value = "/update/user/enable/secured/{verifyVia}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> Authenticate(@PathVariable String verifyVia,@Valid @RequestBody UserModel userToUpdate) {
		if (userToUpdate==null){
			return ResponseEntity.badRequest().body(new MessageResponse("Bad Request !"));
		}else {
			try {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				String userName = principal.toString();
				String userEmail = keyCloakService.getUserWithName(userName);
				if (!userEmail.equals("User Not Found !")) {
					UserModel user = userRepository.findByEmail(userEmail);
					if (user == null) {
						return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
					}else {
						boolean twilioOtpResponse = Twilio2FACodeCheck(user, userToUpdate.getRefId(),verifyVia);
						if(twilioOtpResponse){

							if(user.isTwoFactorFlag())
							{
								user.setTwoFactorFlag(false);
							}
							else
							{
								user.setTwoFactorFlag(true);
							}

							userRepository.save(user);
							return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "user SuccessFully enabled")
									.body(user.isTwoFactorFlag());

						}
						else{
							return ResponseEntity.badRequest().body(new MessageResponse("OTP is invalid or expired"));
						}
					}
				}else {
					return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
				}

			}catch (Exception e){
				System.out.println(e.getMessage());
				return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}


	@ApiOperation(value = "Update User Phone", notes = "Update User Phone")
	@PostMapping(value = "/verify/email/opt/{verifyVia}")
	public ResponseEntity<Object> verifyEmailOtp(@ApiParam(value = "Update User Phone", required = true)
												  @Valid @RequestBody UserModel userToUpdate,
												  @PathVariable("verifyVia") String verifyVia) {
		log.info("inside AuthController.UpdateUserProfile() method");
		if (userToUpdate==null){
			return ResponseEntity.badRequest().body(new MessageResponse("Bad Request !"));
		}else {
			try {

				UserModel userEmail = userRepository.findByEmail(userToUpdate.getEmail());
				if (!userEmail.equals("User Not Found !")) {

					if (userEmail == null) {
						return new ResponseEntity<>("User" + userEmail + " Not Found !", HttpStatus.NOT_FOUND);
					}else {
						boolean twilioOtpResponse = Twilio2FACodeCheck(userEmail, userToUpdate.getRefId(),verifyVia);
						if(twilioOtpResponse){
								userEmail.setVerifiedForPass(true);
								userRepository.save(userEmail);
							return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "verified successfully")
									.body(twilioOtpResponse);
						}
						else{
							return ResponseEntity.badRequest().body(new MessageResponse("OTP is invalid or expired"));
						}
					}
				}else {
					return new ResponseEntity<>("User Not Found, !", HttpStatus.NOT_FOUND);
				}

			}catch (Exception e){
				System.out.println(e.getMessage());
				return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
    /*Phone number update*/
	@ApiOperation(value = "Update User Phone", notes = "Update User Phone")
	@PostMapping(value = "/update/user/{verifyVia}")
	public ResponseEntity<Object> UpdateUserPhone(@ApiParam(value = "Update User Phone", required = true)
													  @Valid @RequestBody UserModel userToUpdate,
												  @PathVariable("verifyVia") String verifyVia) {
		log.info("inside AuthController.UpdateUserProfile() method");
		if (userToUpdate==null){
			return ResponseEntity.badRequest().body(new MessageResponse("Bad Request !"));
		}else {
			try {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				String userName = principal.toString();
				String userEmail = keyCloakService.getUserWithName(userName);
				if (!userEmail.equals("User Not Found !")) {
					UserModel user = userRepository.findByEmail(userEmail);
					if (user == null) {
						return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
					}else {
						boolean twilioOtpResponse = Twilio2FACodeCheck(user, userToUpdate.getRefId(),verifyVia);
						if(twilioOtpResponse){
							boolean emailOrPhoneModified = false;

							String phone = userToUpdate.getPhoneNo();
							String OldPhone = user.getPhoneNo();
							if (phone!=null){
								emailOrPhoneModified = true;
								user.setPhoneNo(phone);
							}

							if (emailOrPhoneModified){
								boolean updateKeycloakUser = keyCloakService.userUpdatePhone(OldPhone, userToUpdate.getPassword(), phone);

								if (updateKeycloakUser){
									System.out.println("updateKeycloakUser");
									userRepository.save(user);

									/*boolean MakeNotificationForUser = notificationService.MakeNotifications(user.getId(), user.getUsername(), "Phone number updated successfully", user.getProfileUrl(), "/setting");
									if (!MakeNotificationForUser){
										log.info("User Notification - unexpected Error !! - inside AuthController.UpdateUserProfile() method");
									}*/

									return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "verified successfully")
											.body(user);
								}else {
									return new ResponseEntity<>("Phone Not updated successfully !", HttpStatus.UNAUTHORIZED);
								}
							}else {
								userRepository.save(user);

								/*boolean MakeNotificationForUser = notificationService.MakeNotifications(user.getId(), user.getUsername(), "profile updated successfully", user.getProfileUrl(), "/setting");
								if (!MakeNotificationForUser){
									log.info("User Notification - unexpected Error !! - inside AuthController.UpdateUserProfile() method");
								}*/

								return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "verified successfully")
										.body(user);
							}
						}
						else{
							return ResponseEntity.badRequest().body(new MessageResponse("OTP is invalid or expired"));
						}
					}
				}else {
					return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
				}

			}catch (Exception e){
				System.out.println(e.getMessage());
				return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@ApiOperation(value = "Update User Email", notes = "Update User Email")
	@PostMapping(value = "/update/user/mail/{verifyVia}")
	public ResponseEntity<Object> UpdateUserEmail(@ApiParam(value = "Update User Phone", required = true) @Valid @RequestBody UserModel userToUpdate, @PathVariable("verifyVia") String verifyVia) {
		log.info("inside AuthController.UpdateUserEmail() method");
		if (userToUpdate==null){
			return ResponseEntity.badRequest().body(new MessageResponse("Bad Request !"));
		}else {
			try {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				String userName = principal.toString();
				String userEmail = keyCloakService.getUserWithName(userName);
				if (!userEmail.equals("User Not Found !")) {
					UserModel user = userRepository.findByEmail(userEmail);
					if (user == null) {
						return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
					}else {
						boolean twilioOtpResponse = Twilio2FACodeCheck(user, userToUpdate.getRefId(),verifyVia);
						if(twilioOtpResponse){
							boolean emailOrPhoneModified = false;

							String email = userToUpdate.getEmail();
							if (email!=null){
								emailOrPhoneModified = true;
								user.setEmail(email);
							}

							if (emailOrPhoneModified){
								boolean updateKeycloakUser = keyCloakService.userUpdateEmail(user.getPhoneNo(), userToUpdate.getPassword(), email);

								if (updateKeycloakUser){
									userRepository.save(user);

									/*boolean MakeNotificationForUser = notificationService.MakeNotifications(user.getId(), user.getUsername(), "Email updated successfully", user.getProfileUrl(), "/setting");
									if (!MakeNotificationForUser){
										log.info("User Notification - unexpected Error !! - inside AuthController.UpdateUserProfile() method");
									}*/

									return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "verified successfully")
											.body(user);
								}else {
									return new ResponseEntity<>("Email Not updated successfully !", HttpStatus.UNAUTHORIZED);
								}
							}else {
								userRepository.save(user);

								/*boolean MakeNotificationForUser = notificationService.MakeNotifications(user.getId(), user.getUsername(), "profile updated successfully", user.getProfileUrl(), "/setting");
								if (!MakeNotificationForUser){
									log.info("User Notification - unexpected Error !! - inside AuthController.UpdateUserProfile() method");
								}*/

								return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "verified successfully")
										.body(user);
							}
						}
						else{
							return ResponseEntity.badRequest().body(new MessageResponse("OTP is invalid or expired"));
						}
					}
				}else {
					return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
				}

			}catch (Exception e){
				System.out.println(e.getMessage());
				return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}


	@ApiOperation(value = "check User Password", notes = "check User Password")
	@GetMapping(value = "/check/user/{userId}/{password}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> checkUserPassword(@PathVariable("userId") String userId,@PathVariable("password") String password) {
		log.info("inside AuthController.checkUserPassword() method");
		try {

				UserModel user = userRepository.findById(userId).orElse(null);
				if (user == null) {
					return new ResponseEntity<>("User" + userId + " Not Found !", HttpStatus.NOT_FOUND);
				}else {
					boolean matches = encoder.matches(password, user.getPassword());
					if (matches){
						return ResponseEntity.ok(new MessageResponse(" verified successfully."));
					}else {
						return new ResponseEntity<>("Incorrect password", HttpStatus.NOT_FOUND);
					}
				}


		}catch (Exception e){
			return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	/*Jun-01*/
	@ApiOperation(value = "Update User Password By token", notes = "Update User Password By token")
	@PostMapping(value = "/user/update/password")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> UpdateUserPasswordByToken(@ApiParam(value = "Update User Password", required = true)
																@Valid @RequestBody ForgetPasswordModel forgetPassword) {
		log.info("inside AuthController.UpdateUserPasswordByToken() method");
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String userName = principal.toString();
			String userEmail = keyCloakService.getUserWithName(userName);
			if (!userEmail.equals("User Not Found !")) {
				UserModel user = userRepository.findByEmail(userEmail);
				if (user == null) {
					return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
				}else {
					boolean matches = encoder.matches(forgetPassword.getCurrentPassword(), user.getPassword());
					if (matches){
						boolean confirmMatches = encoder.matches(forgetPassword.getConfirmPassword(), user.getPassword());
						if (confirmMatches){
							return new ResponseEntity<>("current password and new password shouldn't be same !", HttpStatus.BAD_REQUEST);
						}else {
							boolean updateKeycloakUser = keyCloakService.userUpdate(userName, forgetPassword.getConfirmPassword());
							if (updateKeycloakUser){
								user.setPassword(encoder.encode(forgetPassword.getConfirmPassword()));
								if(user.getPasswordUpdatedCount()==0)
								{
									user.setPasswordUpdatedCount(1);
								}
								else
								{
									user.setPasswordUpdatedCount(user.getPasswordUpdatedCount()+1);
								}
								userRepository.save(user);
								/*boolean MakeNotificationForUser = notificationService.MakeNotifications(user.getId(), user.getUsername(), "Password Reset Success", user.getProfileUrl(), "/setting");
								if (!MakeNotificationForUser){
									log.info("User Notification - unexpected Error !! - inside AuthController.UpdateUserProfile() method");
								}*/
								PermissionModel permissionModel = permissionsRepository.findByUserId(user.getId());
								if (permissionModel.getPermissions().get(0).getSubCategory().get(0).getAttr().getEmail()){
									emailService.sendMail(user.getUsername(),user.getEmail(), "Password Reset Success",
											"Password Reset Success");
								}
								AccessTokenResponse response = keyCloakService.userLogin(userName, forgetPassword.getConfirmPassword());
								if (response.getToken()==null){
									return new ResponseEntity<>("password updated, but we are facing some issue's", HttpStatus.BAD_REQUEST);
								}else {
									return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "password successfully updated")
											.body(response);
								}
							}else {
								return new ResponseEntity<>("Password Not updated successfully !", HttpStatus.UNAUTHORIZED);
							}
						}
					}else {
						return new ResponseEntity<>("current password is Invalid !", HttpStatus.UNAUTHORIZED);
					}
				}
			}else {
				return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.UNAUTHORIZED);
			}

		}catch (Exception e){
			System.out.println(e.getMessage());
			return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*JAN 03*/
	@ApiOperation(value = "check If user already exist", notes = "check If user already exist")
	@GetMapping(value = "/check/user/{mailId}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> checkIfUserAlreadyExist(@ApiParam(value = "check If user already exist", required = true) @PathVariable("mailId") String mailId) {
		log.info("inside AuthController.checkIfUserAlreadyExist() method");
		try{
			if (userRepository.existsByEmail(mailId)){
				return ResponseEntity.ok(true);
			}else {
				return ResponseEntity.ok(false);
			}
		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e.getMessage()));
		}
	}

	@ApiOperation(value = "check If user already exist", notes = "check If user already exist")
	@GetMapping(value = "/check/user/{mailId}/normal")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> checkIfNormalUserAlreadyExist(@ApiParam(value = "check If user already exist", required = true) @PathVariable("mailId") String mailId) {
		log.info("inside AuthController.checkIfUserAlreadyExist() method");
		try{
			if (userRepository.existsByEmailAndLoginType(mailId,"normal")){
				return ResponseEntity.ok(true);
			}else {
				return ResponseEntity.ok(false);
			}
		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e.getMessage()));
		}
	}

	/*JAN 03*/
	@ApiOperation(value = "check If user already exist", notes = "check If user already exist")
	@GetMapping(value = "/check/PhoneNo/{phone}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> checkIfUserPhoneNoAlreadyExist(@ApiParam(value = "check If user already exist", required = true) @PathVariable("phone") String phone) {
		log.info("inside AuthController.checkIfUserAlreadyExist() method");
		try{
			if (userRepository.existsByPhoneNo(phone)){
				return ResponseEntity.ok(true);
			}else {
				return ResponseEntity.ok(false);
			}
		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e.getMessage()));
		}
	}


	@ApiOperation(value = "Get  user Info ", notes = "Get  user Info ")
	@PostMapping(value = "/set/user/tour/{userID}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<?> setTourFlag(@ApiParam(value = "get User Info By Id", required = true) @PathVariable("userID") ObjectId userID) {
		log.info("inside AuthController.getUserInfoById() method");
		try{
			log.info("inside AuthController.getUserInfoById() method");

			UserModel model = userRepository.findById(userID);
			if (model==null) {
				return ResponseEntity.badRequest().header("Custom_Header_of_PlayBid", "search Not Found").body(null);
			}
			else
			{
				model.setViewedTour(true);
				userRepository.save(model);
				return ResponseEntity.ok().header("users", "Get All user Successfully")
						.body(model);
			}


		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
		}
	}



	@ApiOperation(value = "Get  user Info ", notes = "Get  user Info ")
	@GetMapping(value = "/get/user/{userID}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<?> getUserInfoById(@ApiParam(value = "get User Info By Id", required = true) @PathVariable("userID") ObjectId userID) {
		log.info("inside AuthController.getUserInfoById() method");
		try{
			log.info("inside AuthController.getUserInfoById() method");

			UserModel model = userRepository.findById(userID);
			if (model==null) {
				return ResponseEntity.badRequest().header("Custom_Header_of_PlayBid", "search Not Found").body(null);
			}
			else
			{
				return ResponseEntity.ok().header("users", "Get All user Successfully")
						.body(model);
			}


		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
		}
	}


	@ApiOperation(value = "Delete Users Record by Id", notes = "DELETE API to Delete Users Object By Id.")
	@DeleteMapping(value = "/delete/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Object> deleteById(@ApiParam(value = "DeleteByIdRequest Object", required = true) @PathVariable("id") ObjectId id) {
		try{
			log.info("inside AuthController.deleteById() method");

			UserModel model = userRepository.findById(id);
			if (!userRepository.existsById(id)) {
				return new ResponseEntity<Object>("User Id " + id + " does not exists", HttpStatus.NOT_FOUND);
			}
			UserModel model2 = userRepository.findById(id);
			model2.setDeleted(true);
			userRepository.save(model2);
			return ResponseEntity.ok(new MessageResponse("User Name " + model.getUsername() + " Deleted Successfully"));
		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
		}
	}

	@ApiOperation(value = "Confirm Account", notes = "API to verify the user.")
	@RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
	public ResponseEntity<Object> confirmUserAccount(@ApiParam(value = "ConfirmAccount Object", required = true) ModelAndView modelAndView, @RequestParam("token") String confirmationToken, HttpServletRequest request) throws MessagingException {
		try {
			VerifiedTokenModel token = verifiedTokenRepository.findByToken(confirmationToken);

			if (token == null) {
				modelAndView.addObject("message", "The link is invalid or broken!");
				modelAndView.setViewName("error");
				return ResponseEntity.badRequest().body(new MessageResponse("The link is invalid or broken!!"));

			} else if (token.isExpired()) {
				modelAndView.addObject("message", "Token Expired!");
				modelAndView.setViewName("error");
				return ResponseEntity.badRequest().body(new MessageResponse("The link is Expired!"));
			} else {
				UserModel user = userRepository.findByEmailIgnoreCase(token.getUser().getEmail());
				user.setVerified(true);
				userRepository.save(user);
				modelAndView.setViewName("accountVerified");
				verifiedTokenRepository.deleteById(token.getId());
				String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
				emailService.sendMail(user.getUsername(), user.getEmail(), "Welcome to Play-crypto", "");
				return ResponseEntity.ok(new MessageResponse("Welcome to Play-crypto.Your account has been verified successfully."));
			}
		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
		}

	}


	@GetMapping(value = "/notify/getAll")
	public ResponseEntity<Object> getNotifyList(@PageableDefault(page = 0, size = 10, sort = "emailEnteredTime", direction = Sort.Direction.DESC) Pageable pageable)
	{
	Page<NotifyMeModel> model = notifyMeRepository.findAll(pageable);

		if (model == null){
			return  ResponseEntity.badRequest().body("Users not found");

		}else {

			return ResponseEntity.ok().header("users", "Get All users  Successfully")
					.body(model);
		}
	}

	@ApiOperation(value = "Mail Reset Password Token", notes = "POST API for process mail reset password token object.")
	@PostMapping(value = "/mail-reset-password-token")
	public ResponseEntity<Object> mailTokenForResetPassword(@RequestBody UserModel resetMail) throws MessagingException {
		try {
			log.info("inside AuthController.mailTokenForResetPassword() method");
			UserModel user = userRepository.findByEmail(resetMail.getEmail());

			if (user == null) {
				return ResponseEntity.badRequest().body(new MessageResponse("Email Id does not exists!"));
			}
			if (user.isVerifiedForPass()) {
			boolean updateKeycloakUser = keyCloakService.userUpdate(user.getPhoneNo(), resetMail.getPassword());
			if (updateKeycloakUser) {

				user.setPassword(encoder.encode(resetMail.getPassword())) ;
				user.setVerifiedForPass(false);
				userRepository.save(user);
				PermissionModel permissionModel = permissionsRepository.findByUserId(user.getId());
				if (permissionModel.getPermissions().get(0).getSubCategory().get(0).getAttr().getEmail()) {
					emailService.sendMail(user.getUsername(), user.getEmail(), "Initiate Reset Password",
							"Initiate Reset Password");
				}
				return ResponseEntity.ok(new MessageResponse("Password updates."));
			}else
			{
				return ResponseEntity.badRequest().body(new MessageResponse("Password update failed"));
			}

		}else
			{
				return ResponseEntity.badRequest().body(new MessageResponse("Invalid request"));
			}
		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
		}
	}


	@ApiOperation(value = "Forgot Password", notes = "API for process forgot password token object.")
	@RequestMapping(value = "/forget-account-password", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView resetUserAccountPassword(@ApiParam(value = "ForgotAccountPassword Object", required = true) ModelAndView modelAndView, @RequestParam("token") String resetToken) {
		PasswordResetTokenModel token = passwordResetTokenRepository.findByToken(resetToken);

		if (token == null) {
			modelAndView.addObject("message", "The link is invalid or broken!");
			modelAndView.setViewName("error");

		} else if (token.isExpired()) {
			modelAndView.addObject("message", "Token Expired!");
			modelAndView.setViewName("error");
		} else {
			UserModel user = userRepository.findByEmailIgnoreCase(token.getUser().getEmail());

			modelAndView.addObject("user", user);
			modelAndView.addObject("emailId", user.getEmail());
			modelAndView.setViewName("resetPassword");
			passwordResetTokenRepository.deleteById(token.getId());
		}

		return modelAndView;
	}

	@ApiOperation(value = "Reset Password", notes = "API for process reset password token object.")
	@RequestMapping(value = "/reset-password", method = RequestMethod.POST)
	public ResponseEntity resetUserPassword(@ApiParam(value = "ResetPassword Object", required = true) @RequestParam("token") String resetToken, @RequestBody String newPassword) {
		try {
			PasswordResetTokenModel token = passwordResetTokenRepository.findByToken(resetToken);

			if (token == null) {

				return ResponseEntity.badRequest().body("The link is invalid or broken!");

			} else if (token.isExpired()) {
				return ResponseEntity.badRequest().body("The link is expired.!");

			} else {
				UserModel user = userRepository.findByEmailIgnoreCase(token.getUser().getEmail());
				boolean updateKeycloakUser = keyCloakService.userUpdate(user.getPhoneNo(), newPassword);
				if (updateKeycloakUser) {
					passwordResetTokenRepository.deleteById(token.getId());
					user.setPassword(encoder.encode(newPassword));
					userRepository.save(user);
					PermissionModel permissionModel = permissionsRepository.findByUserId(user.getId());
					if (permissionModel.getPermissions().get(0).getSubCategory().get(1).getAttr().getEmail()) {
						emailService.sendMail(user.getUsername(), user.getEmail(), "Password Reset Success",
								"Password Reset Success");
						if (user.getPasswordResetCount() == 0) {
							user.setPasswordResetCount(1);
						} else {
							user.setPasswordResetCount(user.getPasswordResetCount() + 1);
						}
						userRepository.save(user);
					}
					return ResponseEntity.ok(new MessageResponse("Password reset successful."));

				}else {
					return ResponseEntity.badRequest().body(new MessageResponse("Unable to reset password"));
				}
			}
		} catch (Exception e){
			return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
		}
	}

	/*Jul 09*/
	@ApiOperation(value = "Get Overall Progress For Admin", notes = "Get Overall Progress For Admin")
	@GetMapping(value = "/get/overall/progress")
	public ResponseEntity<Object> getOverallProgressForAdmin() {
		log.info("inside AuthController.getOverallProgressForAdmin() method");
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String userName = principal.toString();
			String userEmail = keyCloakService.getUserWithName(userName);
			if (!userEmail.equals("User Not Found !")) {
				UserModel user = userRepository.findByEmail(userEmail);
				if (user == null) {
					return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
				}else {
					List<OverallProgressModal> overallProgressModal = overallProgressRepository.findAll();
					if (overallProgressModal.size() == 0){
						return new ResponseEntity<>("No Progress Yet !", HttpStatus.NOT_FOUND);
					}else {
						return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Overall Progress Success")
								.body(overallProgressModal.get(0));
					}
				}
			}else {
				return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
			}

		}catch (Exception e){
			return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public boolean setInitPermissions(String userId){
		log.info("inside PermissionController.setInitPermissions() method");
		try {
			PermissionModel permissionModel = new PermissionModel();
			permissionModel.setUserId(userId);
			PermitNotificationModel permitNotificationModel = new PermitNotificationModel(false, false,false);
			PermitNotificationModel permitNotificationModelEmailTrue = new PermitNotificationModel(true, false,false);
			List<String> allSubCategoryOfGeneral = new ArrayList<>();
			List<String> allSubCategoryOfBids = new ArrayList<>();
			allSubCategoryOfGeneral.add("Password Reset successful");
			allSubCategoryOfGeneral.add("Forgot Password Request initiated");
			allSubCategoryOfGeneral.add("Latest Updates, Newsletters");
			allSubCategoryOfBids.add("Bid Purchase successful");
			allSubCategoryOfBids.add("Bid draw results");
			allSubCategoryOfBids.add("New Bid open notification");

			List<PermissionCategoryModel> permissionCategoryModelList = new ArrayList<>();
			for(int i=0; i<2;i++){
				PermissionCategoryModel permissionCategoryModel = new PermissionCategoryModel();
				if (i==0){
					permissionCategoryModel.setCategory("General");
					List<PermissionSubCategoryModel> permissionSubCategoryModelList = new ArrayList<>();
					for(String SubCategory : allSubCategoryOfGeneral){
						PermissionSubCategoryModel permissionSubCategoryModel = new PermissionSubCategoryModel();
						permissionSubCategoryModel.setName(SubCategory);
						if(SubCategory.equals("Password Reset successful")
								|| SubCategory.equals("Forgot Password Request initiated")){
							permissionSubCategoryModel.setAttr(permitNotificationModelEmailTrue);
						}else {
							permissionSubCategoryModel.setAttr(permitNotificationModel);
						}
						permissionSubCategoryModelList.add(permissionSubCategoryModel);
					}
					permissionCategoryModel.setSubCategory(permissionSubCategoryModelList);
				}else{
					permissionCategoryModel.setCategory("Bids");
					List<PermissionSubCategoryModel> permissionSubCategoryModelList = new ArrayList<>();
					for(String SubCategory : allSubCategoryOfBids){
						PermissionSubCategoryModel permissionSubCategoryModel = new PermissionSubCategoryModel();
						permissionSubCategoryModel.setName(SubCategory);
						if(SubCategory.equals("Bid Purchase successful")){
							permissionSubCategoryModel.setAttr(permitNotificationModelEmailTrue);
						}else {
							permissionSubCategoryModel.setAttr(permitNotificationModel);
						}
						permissionSubCategoryModelList.add(permissionSubCategoryModel);
					}
					permissionCategoryModel.setSubCategory(permissionSubCategoryModelList);
				}
				permissionCategoryModelList.add(permissionCategoryModel);
			}
			permissionModel.setPermissions(permissionCategoryModelList);
			permissionsRepository.save(permissionModel);
			return true;
		}catch (Exception e){
			System.out.println(e.getMessage());
			return false;
		}
	}

}