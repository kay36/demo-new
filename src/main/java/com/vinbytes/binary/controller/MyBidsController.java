package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.MyBidsModel;
import com.vinbytes.binary.model.PlayBidModel;
import com.vinbytes.binary.model.PurchaseBidsModel;
import com.vinbytes.binary.repository.MyBidsRepository;
import com.vinbytes.binary.repository.PlayBidRepository;
import com.vinbytes.binary.repository.PurchaseBidsRepository;
import com.vinbytes.binary.services.MyBidsServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "My Bids Controller")
@CrossOrigin("*")
@RestController
@RequestMapping(value="/mybids")
public class MyBidsController extends MyBidsServices {

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private MyBidsRepository myBidsRepository;

    @Autowired
    public PurchaseBidsRepository purchaseBidsRepository;

    @Autowired
    public PlayBidRepository playBidRepository;

    @Autowired
    private MyBidsModel myBidsModel;

    @Autowired
    private PlayBidModel playBidModel;

    @Autowired
    private PurchaseBidsModel purchaseBidsModel;

    private static final Logger log = LoggerFactory.getLogger(PlayBidController.class);

    @ApiOperation(value = "Get All Bids", notes = "GET API to get all bids.")
    @GetMapping(value = "/getAll")
    @ApiParam(value = "MyBids Object", required = true)
    public HttpEntity<? extends Object> getAllMyBids() {
        try {
            log.info("inside MyBidsController.getAllMyBids() method");
            List<PurchaseBidsModel> purchase = purchaseBidsRepository.findAll();
            List<PlayBidModel> play = playBidRepository.findAll();
            MyBidsModel myBids;
            myBidsRepository.insert(myBidsModel);
            if (myBidsModel == null) {
                return ResponseEntity.badRequest().header("Custom_Header_of_MyBids", "My bids Not Found").body(myBidsModel);
            } else {
                return ResponseEntity.ok().header("Custom_Header_of_MyBids", "Get All My Bids Successfully")
                        .body(myBidsModel);
            }
        } catch (Exception e){
            return ResponseEntity.badRequest().body(new MessageResponse("Error info "+e));
        }
    }
}
