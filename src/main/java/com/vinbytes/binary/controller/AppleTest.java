package com.vinbytes.binary.controller;

import com.vinbytes.binary.model.AppleResponseModel;
import io.jsonwebtoken.io.Decoders;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
public class AppleTest {

    @RequestMapping(path="/home", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public void method(HttpServletResponse httpServletResponse, @RequestBody AppleResponseModel payload) {
        System.out.println(payload.getCode());
        System.out.println(payload.getId_token());

        String idToken = payload.getId_token();
        String payload1 = idToken.split("\\.")[1];//0 is header we ignore it for now
        String decoded = new String(Decoders.BASE64.decode(payload1));
        System.out.println(decoded);

        Cookie cookie = new Cookie("username", "Kishor");
        httpServletResponse.addCookie(cookie);
        httpServletResponse.setHeader("Location", "https://google.co.in");
        httpServletResponse.setStatus(302);
    }
}
