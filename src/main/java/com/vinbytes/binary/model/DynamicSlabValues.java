package com.vinbytes.binary.model;

public class DynamicSlabValues {

    private Double fromValue;
    private Double toValue;

    public DynamicSlabValues() {
    }

    public DynamicSlabValues(Double fromValue, Double toValue) {
        this.fromValue = fromValue;
        this.toValue = toValue;
    }

    public Double getFromValue() {
        return fromValue;
    }

    public void setFromValue(Double fromValue) {
        this.fromValue = fromValue;
    }

    public Double getToValue() {
        return toValue;
    }

    public void setToValue(Double toValue) {
        this.toValue = toValue;
    }
}
