package com.vinbytes.binary.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "coinPaymentsResponse")
public class coinPaymentsTransactionStatusResponseModel {

    @Id
    private String id;
    private String coinPaymentsId;
    private Date dateAndTime;
    private List<IPNFRFModel> ipnfrf;
    private List<IPNFWFModel> ipnfwf;
    private long status;
    private String statusText;

    public coinPaymentsTransactionStatusResponseModel() {
    }

    public coinPaymentsTransactionStatusResponseModel(String id, String coinPaymentsId, Date dateAndTime, List<IPNFRFModel> ipnfrf, List<IPNFWFModel> ipnfwf, long status, String statusText) {
        this.id = id;
        this.coinPaymentsId = coinPaymentsId;
        this.dateAndTime = dateAndTime;
        this.ipnfrf = ipnfrf;
        this.ipnfwf = ipnfwf;
        this.status = status;
        this.statusText = statusText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoinPaymentsId() {
        return coinPaymentsId;
    }

    public void setCoinPaymentsId(String coinPaymentsId) {
        this.coinPaymentsId = coinPaymentsId;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public List<IPNFRFModel> getIpnfrf() {
        return ipnfrf;
    }

    public void setIpnfrf(List<IPNFRFModel> ipnfrf) {
        this.ipnfrf = ipnfrf;
    }

    public List<IPNFWFModel> getIpnfwf() {
        return ipnfwf;
    }

    public void setIpnfwf(List<IPNFWFModel> ipnfwf) {
        this.ipnfwf = ipnfwf;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }
}

