package com.vinbytes.binary.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;


@ApiModel(description = "Transaction Log Maintenance")
@Document(collection = "CoinBaseTransactionLog")
public class CoinBaseTransactionModal {

    @Id
    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "Transaction id", required = true)
    private String transactionId;

    @ApiModelProperty(value = "Transaction address", required = true)
    private String transactionAddress;

    @ApiModelProperty(value = "CoinBase Transaction id", required = true)
    private String coinBaseTransactionId;

    @ApiModelProperty(value = "CoinBase Transaction Hosted Url", required = true)
    private String coinBaseTransactionHostedUrl;

    @ApiModelProperty(value = "CoinBase Transaction Hosted Url Expires", required = true)
    private Date coinBaseTransactionHostedUrlExpires;

    @ApiModelProperty(value = "dateAndTime", required = true)
    private Date transactionInitiateDateAndTime;

    @ApiModelProperty(value = "Total Amount", required = true)
    private Double totalAmount = 0.00;

    @ApiModelProperty(value = "convertedCurrencyType", required = true)
    private String convertedCurrencyType;

    @ApiModelProperty(value = "convertedCurrencyAmount", required = true)
    private String convertedCurrencyAmount;

    @ApiModelProperty(value = "transferredCurrencyType", required = true)
    private String transferredCurrencyType;

    @ApiModelProperty(value = "transferredCurrencyAmount", required = true)
    private String transferredCurrencyAmount;


    @ApiModelProperty(value = "User id", required = true)
    private String userId;

    @ApiModelProperty(value = "User name", required = true)
    private String userName;

    @ApiModelProperty(value = "Stock id", required = true)
    private String stockId;

    @ApiModelProperty(value = "Stock name", required = true)
    private String stockName;

    @ApiModelProperty(value = "Stock Symbol", required = true)
    private String stockSymbol;

    @ApiModelProperty(value = "Transaction Mode", required = true)
    private String transactionMode;

    @ApiModelProperty(value = "Transaction Type", required = true)
    private String transactionType;

    @ApiModelProperty(value = "Transaction status", required = true)
    private String transactionStatus;

    @ApiModelProperty(value = "Transaction status", required = true)
    private String unitsPurchased;

    private Boolean receivedPayment=false;

    private String confirmReceivedPayment;

    private String userPurchaseId;

    private Date paymentCreatedAt;

    private Date paymentReceivedAt;

    private String ChangesBtw;

    private String level;

    private String valueOfOneBid;

    private String transactionFee;
    private String biddingAmount;

    public CoinBaseTransactionModal(String id, String transactionId, String transactionAddress, String coinBaseTransactionId, String coinBaseTransactionHostedUrl, Date coinBaseTransactionHostedUrlExpires, Date transactionInitiateDateAndTime, Double totalAmount, String convertedCurrencyType, String convertedCurrencyAmount, String transferredCurrencyType, String transferredCurrencyAmount, String userId, String userName, String stockId, String stockName, String stockSymbol, String transactionMode, String transactionType, String transactionStatus, String unitsPurchased, Boolean receivedPayment, String confirmReceivedPayment, String userPurchaseId, Date paymentCreatedAt, Date paymentReceivedAt, String changesBtw, String level, String valueOfOneBid, String transactionFee, String biddingAmount) {
        this.id = id;
        this.transactionId = transactionId;
        this.transactionAddress = transactionAddress;
        this.coinBaseTransactionId = coinBaseTransactionId;
        this.coinBaseTransactionHostedUrl = coinBaseTransactionHostedUrl;
        this.coinBaseTransactionHostedUrlExpires = coinBaseTransactionHostedUrlExpires;
        this.transactionInitiateDateAndTime = transactionInitiateDateAndTime;
        this.totalAmount = totalAmount;
        this.convertedCurrencyType = convertedCurrencyType;
        this.convertedCurrencyAmount = convertedCurrencyAmount;
        this.transferredCurrencyType = transferredCurrencyType;
        this.transferredCurrencyAmount = transferredCurrencyAmount;
        this.userId = userId;
        this.userName = userName;
        this.stockId = stockId;
        this.stockName = stockName;
        this.stockSymbol = stockSymbol;
        this.transactionMode = transactionMode;
        this.transactionType = transactionType;
        this.transactionStatus = transactionStatus;
        this.unitsPurchased = unitsPurchased;
        this.receivedPayment = receivedPayment;
        this.confirmReceivedPayment = confirmReceivedPayment;
        this.userPurchaseId = userPurchaseId;
        this.paymentCreatedAt = paymentCreatedAt;
        this.paymentReceivedAt = paymentReceivedAt;
        ChangesBtw = changesBtw;
        this.level = level;
        this.valueOfOneBid = valueOfOneBid;
        this.transactionFee = transactionFee;
        this.biddingAmount = biddingAmount;
    }

    public Date getPaymentReceivedAt() {
        return paymentReceivedAt;
    }

    public void setPaymentReceivedAt(Date paymentReceivedAt) {
        this.paymentReceivedAt = paymentReceivedAt;
    }

    public CoinBaseTransactionModal() {
    }

    public String getTransactionFee() {
        return transactionFee;
    }

    public void setTransactionFee(String transactionFee) {
        this.transactionFee = transactionFee;
    }

    public String getBiddingAmount() {
        return biddingAmount;
    }

    public void setBiddingAmount(String biddingAmount) {
        this.biddingAmount = biddingAmount;
    }

    public String getChangesBtw() {
        return ChangesBtw;
    }

    public void setChangesBtw(String changesBtw) {
        ChangesBtw = changesBtw;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getValueOfOneBid() {
        return valueOfOneBid;
    }

    public void setValueOfOneBid(String valueOfOneBid) {
        this.valueOfOneBid = valueOfOneBid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Boolean getReceivedPayment() {
        return receivedPayment;
    }

    public void setReceivedPayment(Boolean receivedPayment) {
        this.receivedPayment = receivedPayment;
    }

    public String getConfirmReceivedPayment() {
        return confirmReceivedPayment;
    }

    public void setConfirmReceivedPayment(String confirmReceivedPayment) {
        this.confirmReceivedPayment = confirmReceivedPayment;
    }

    public String getTransactionAddress() {
        return transactionAddress;
    }

    public void setTransactionAddress(String transactionAddress) {
        this.transactionAddress = transactionAddress;
    }

    public String getCoinBaseTransactionId() {
        return coinBaseTransactionId;
    }

    public void setCoinBaseTransactionId(String coinBaseTransactionId) {
        this.coinBaseTransactionId = coinBaseTransactionId;
    }

    public String getUserPurchaseId() {
        return userPurchaseId;
    }

    public void setUserPurchaseId(String userPurchaseId) {
        this.userPurchaseId = userPurchaseId;
    }

    public Date getPaymentCreatedAt() {
        return paymentCreatedAt;
    }

    public void setPaymentCreatedAt(Date paymentCreatedAt) {
        this.paymentCreatedAt = paymentCreatedAt;
    }

    public String getCoinBaseTransactionHostedUrl() {
        return coinBaseTransactionHostedUrl;
    }

    public void setCoinBaseTransactionHostedUrl(String coinBaseTransactionHostedUrl) {
        this.coinBaseTransactionHostedUrl = coinBaseTransactionHostedUrl;
    }

    public Date getCoinBaseTransactionHostedUrlExpires() {
        return coinBaseTransactionHostedUrlExpires;
    }

    public void setCoinBaseTransactionHostedUrlExpires(Date coinBaseTransactionHostedUrlExpires) {
        this.coinBaseTransactionHostedUrlExpires = coinBaseTransactionHostedUrlExpires;
    }

    public Date getTransactionInitiateDateAndTime() {
        return transactionInitiateDateAndTime;
    }

    public void setTransactionInitiateDateAndTime(Date transactionInitiateDateAndTime) {
        this.transactionInitiateDateAndTime = transactionInitiateDateAndTime;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getConvertedCurrencyType() {
        return convertedCurrencyType;
    }

    public void setConvertedCurrencyType(String convertedCurrencyType) {
        this.convertedCurrencyType = convertedCurrencyType;
    }

    public String getConvertedCurrencyAmount() {
        return convertedCurrencyAmount;
    }

    public void setConvertedCurrencyAmount(String convertedCurrencyAmount) {
        this.convertedCurrencyAmount = convertedCurrencyAmount;
    }

    public String getTransferredCurrencyType() {
        return transferredCurrencyType;
    }

    public void setTransferredCurrencyType(String transferredCurrencyType) {
        this.transferredCurrencyType = transferredCurrencyType;
    }

    public String getTransferredCurrencyAmount() {
        return transferredCurrencyAmount;
    }

    public void setTransferredCurrencyAmount(String transferredCurrencyAmount) {
        this.transferredCurrencyAmount = transferredCurrencyAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public void setStockSymbol(String stockSymbol) {
        this.stockSymbol = stockSymbol;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getUnitsPurchased() {
        return unitsPurchased;
    }

    public void setUnitsPurchased(String unitsPurchased) {
        this.unitsPurchased = unitsPurchased;
    }
}
