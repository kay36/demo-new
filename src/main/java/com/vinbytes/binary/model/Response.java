package com.vinbytes.binary.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.domain.Page;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    //properties
    private Integer count;
    private String status;
    private String responseMessage;
    private List data;
    private Page pageData;
    private String token;
    private Number otp;
    //Constructors

    public Response(Integer count, String status, String responseMessage, List data, Page pageData, String token, Number otp) {
        this.count = count;
        this.status = status;
        this.responseMessage = responseMessage;
        this.data = data;
        this.pageData = pageData;
        this.token = token;
        this.otp = otp;
    }

    public Response(String responseMessage){
        this.responseMessage = responseMessage;
    }

    public Response() {

    }
    //getterSetters
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Page getPageData() {
        return pageData;
    }

    public void setPageData(Page pageData) {
        this.pageData = pageData;
    }

    public Number getOtp() {
        return otp;
    }

    public void setOtp(Number otp) {
        this.otp = otp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public Integer getCount() { return count; }

    public void setCount(Integer count) { this.count = count; }
}

