package com.vinbytes.binary.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Document(collection = "mybids")
public class MyBidsModel {
    @Id
    private String id;
    private String bidID;
    private String stockName;
    private String biddingStartsAt;
    private String biddingClosesAt;
    private String drawTime;
    private String stockValue;
    private String amountInvested;
    private String change;
    private String biddingStatus;
    private LocalDateTime biddingDate;

    public MyBidsModel(){

    }
    public MyBidsModel(String id, String bidID, String stockName, String biddingStartsAt, String biddingClosesAt, String drawTime, String stockValue, String amountInvested, String change, String biddingStatus, LocalDateTime biddingDate) {
//    public MyBidsModel(String id, String bidID, String stockName, String biddingStartsAt, LocalDateTime biddingDate) {
        this.id = id;
        this.bidID = bidID;
        this.stockName = stockName;
        this.biddingStartsAt = biddingStartsAt;
        this.biddingClosesAt = biddingClosesAt;
        this.drawTime = drawTime;
        this.stockValue = stockValue;
        this.amountInvested = amountInvested;
        this.change = change;
        this.biddingStatus = biddingStatus;
        this.biddingDate = biddingDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBidID() {
        return bidID;
    }

    public void setBidID(String bidID) {
        this.bidID = bidID;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getBiddingStartsAt() {
        return biddingStartsAt;
    }

    public void setBiddingStartsAt(String biddingStartsAt) {
        this.biddingStartsAt = biddingStartsAt;
    }

    public String getBiddingClosesAt() {
        return biddingClosesAt;
    }

    public void setBiddingClosesAt(String biddingClosesAt) {
        this.biddingClosesAt = biddingClosesAt;
    }

    public String getDrawTime() {
        return drawTime;
    }

    public void setDrawTime(String drawTime) {
        this.drawTime = drawTime;
    }

    public String getStockValue() {
        return stockValue;
    }

    public void setStockValue(String stockValue) {
        this.stockValue = stockValue;
    }

    public String getBiddingStatus() {
        return biddingStatus;
    }

    public void setBiddingStatus(String biddingStatus) {
        this.biddingStatus = biddingStatus;
    }

    public String getAmountInvested() {
        return amountInvested;
    }

    public void setAmountInvested(String amountInvested) {
        this.amountInvested = amountInvested;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public LocalDateTime getBiddingDate() {
        return biddingDate;
    }

    public void setBiddingDate(LocalDateTime biddingDate) {
        this.biddingDate = biddingDate;
    }

    @Override
    public String toString() {
//        return "PlayBidModel{" +
//                "id='" + id + '\'' +
//                ", bidID='" + bidID + '\'' +
//                ", stockName='" + stockName + '\'' +
//                ", biddingStartsAt='" + biddingStartsAt + '\'' +
//                ", biddingDate='" + biddingDate + '\'' +
//                '}';

        return "PlayBidModel{" +
                "id='" + id + '\'' +
                ", bidID='" + bidID + '\'' +
                ", stockName='" + stockName + '\'' +
                ", biddingStartsAt='" + biddingStartsAt + '\'' +
                ", biddingClosesAt='" + biddingClosesAt + '\'' +
                ", drawTime='" + drawTime + '\'' +
                ", stockValue='" + stockValue + '\'' +
                ", valueOfOneBid='" + amountInvested + '\'' +
                ", noOfBidders='" + change + '\'' +
                ", biddingStatus='" + biddingStatus + '\'' +
                ", biddingDate='" + biddingDate + '\'' +
                '}';
    }
}