package com.vinbytes.binary.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/*Jul 09*/
@ApiModel(description = "Transaction Log Maintenance")
@Document(collection = "TransactionLog")
public class TransactionModal {

    @Id
    @ApiModelProperty(value = "id", required = true)
    private String id;

    private String walletId;
    private String walletType;

    @ApiModelProperty(value = "Transaction id", required = true)
    private String transactionId;

    @ApiModelProperty(value = "Transaction address", required = true)
    private String transactionAddress;

    @ApiModelProperty(value = "CoinBase Transaction id", required = true)
    private String coinBaseTransactionId;

    @ApiModelProperty(value = "CoinBase Transaction Hosted Url", required = true)
    private String coinBaseTransactionHostedUrl;

    @ApiModelProperty(value = "CoinBase Transaction Hosted Url Expires", required = true)
    private Date coinBaseTransactionHostedUrlExpires;

    @ApiModelProperty(value = "dateAndTime", required = true)
    private Date transactionInitiateDateAndTime;

    @ApiModelProperty(value = "Total Amount", required = true)
    private Double totalAmount = 0.00;

    @ApiModelProperty(value = "Bid Amount", required = true)
    private Double bidAmount = 0.00;

    @ApiModelProperty(value = "Transaction Fee Amount", required = true)
    private Double transactionFeeAmount = 0.00;

    @ApiModelProperty(value = "User id", required = true)
    private String userId;

    @ApiModelProperty(value = "User name", required = true)
    private String userName;

    @ApiModelProperty(value = "Stock id", required = true)
    private String stockId;

    @ApiModelProperty(value = "Stock name", required = true)
    private String stockName;

    @ApiModelProperty(value = "Stock Symbol", required = true)
    private String stockSymbol;

    @ApiModelProperty(value = "Transaction Mode", required = true)
    private String transactionMode;

    @ApiModelProperty(value = "Transaction Type", required = true)
    private String transactionType;

    @ApiModelProperty(value = "Transaction status", required = true)
    private String transactionStatus;

    @ApiModelProperty(value = "units Purchased", required = true)
    private String unitsPurchased;

    @ApiModelProperty(value = "creditedUnitBtc", required = true)
    private Double creditedUnitBtc;


    @ApiModelProperty(value = "creditedUnitBtc", required = true)
    private Double receivedAmountFromCoinBase;

    private coinPaymentsDepositResponseModel coinPaymentsDepositResponse;

    private coinPaymentsWithdrawalResponseModel coinPaymentsWithdrawalResponse;

    private Double receivedAmount = 0.00;

    private Double exchangeRate1BTC = 0.00;

    private String exchangeRateString;

    private List<transactionStatusModel> transactionStatusList;

    private boolean transactionState;




    public TransactionModal() {
    }

    public TransactionModal(String id, String walletId, String walletType, String transactionId, String transactionAddress, String coinBaseTransactionId, String coinBaseTransactionHostedUrl, Date coinBaseTransactionHostedUrlExpires, Date transactionInitiateDateAndTime, Double totalAmount, Double bidAmount, Double transactionFeeAmount, String userId, String userName, String stockId, String stockName, String stockSymbol, String transactionMode, String transactionType, String transactionStatus, String unitsPurchased, Double creditedUnitBtc, Double receivedAmountFromCoinBase, coinPaymentsDepositResponseModel coinPaymentsDepositResponse, coinPaymentsWithdrawalResponseModel coinPaymentsWithdrawalResponse, Double receivedAmount, Double exchangeRate1BTC, String exchangeRateString, List<transactionStatusModel> transactionStatusList, boolean transactionState) {
        this.id = id;
        this.walletId = walletId;
        this.walletType = walletType;
        this.transactionId = transactionId;
        this.transactionAddress = transactionAddress;
        this.coinBaseTransactionId = coinBaseTransactionId;
        this.coinBaseTransactionHostedUrl = coinBaseTransactionHostedUrl;
        this.coinBaseTransactionHostedUrlExpires = coinBaseTransactionHostedUrlExpires;
        this.transactionInitiateDateAndTime = transactionInitiateDateAndTime;
        this.totalAmount = totalAmount;
        this.bidAmount = bidAmount;
        this.transactionFeeAmount = transactionFeeAmount;
        this.userId = userId;
        this.userName = userName;
        this.stockId = stockId;
        this.stockName = stockName;
        this.stockSymbol = stockSymbol;
        this.transactionMode = transactionMode;
        this.transactionType = transactionType;
        this.transactionStatus = transactionStatus;
        this.unitsPurchased = unitsPurchased;
        this.creditedUnitBtc = creditedUnitBtc;
        this.receivedAmountFromCoinBase = receivedAmountFromCoinBase;
        this.coinPaymentsDepositResponse = coinPaymentsDepositResponse;
        this.coinPaymentsWithdrawalResponse = coinPaymentsWithdrawalResponse;
        this.receivedAmount = receivedAmount;
        this.exchangeRate1BTC = exchangeRate1BTC;
        this.exchangeRateString = exchangeRateString;
        this.transactionStatusList = transactionStatusList;
        this.transactionState = transactionState;
    }

    public Double getExchangeRate1BTC() {
        return exchangeRate1BTC;
    }

    public void setExchangeRate1BTC(Double exchangeRate1BTC) {
        this.exchangeRate1BTC = exchangeRate1BTC;
    }

    public Double getReceivedAmountFromCoinBase() {
        return receivedAmountFromCoinBase;
    }

    public void setReceivedAmountFromCoinBase(Double receivedAmountFromCoinBase) {
        this.receivedAmountFromCoinBase = receivedAmountFromCoinBase;
    }

    public String getUnitsPurchased() {
        return unitsPurchased;
    }

    public void setUnitsPurchased(String unitsPurchased) {
        this.unitsPurchased = unitsPurchased;
    }

    public Double getCreditedUnitBtc() {
        return creditedUnitBtc;
    }

    public void setCreditedUnitBtc(Double creditedUnitBtc) {
        this.creditedUnitBtc = creditedUnitBtc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCoinBaseTransactionId() {
        return coinBaseTransactionId;
    }

    public String getCoinBaseTransactionHostedUrl() {
        return coinBaseTransactionHostedUrl;
    }

    public void setCoinBaseTransactionHostedUrl(String coinBaseTransactionHostedUrl) {
        this.coinBaseTransactionHostedUrl = coinBaseTransactionHostedUrl;
    }

    public Date getCoinBaseTransactionHostedUrlExpires() {
        return coinBaseTransactionHostedUrlExpires;
    }

    public void setCoinBaseTransactionHostedUrlExpires(Date coinBaseTransactionHostedUrlExpires) {
        this.coinBaseTransactionHostedUrlExpires = coinBaseTransactionHostedUrlExpires;
    }

    public void setCoinBaseTransactionId(String coinBaseTransactionId) {
        this.coinBaseTransactionId = coinBaseTransactionId;
    }

    public Date getTransactionInitiateDateAndTime() {
        return transactionInitiateDateAndTime;
    }

    public void setTransactionInitiateDateAndTime(Date transactionInitiateDateAndTime) {
        this.transactionInitiateDateAndTime = transactionInitiateDateAndTime;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(Double bidAmount) {
        this.bidAmount = bidAmount;
    }

    public Double getTransactionFeeAmount() {
        return transactionFeeAmount;
    }

    public void setTransactionFeeAmount(Double transactionFeeAmount) {
        this.transactionFeeAmount = transactionFeeAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public void setStockSymbol(String stockSymbol) {
        this.stockSymbol = stockSymbol;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionAddress() {
        return transactionAddress;
    }

    public void setTransactionAddress(String transactionAddress) {
        this.transactionAddress = transactionAddress;
    }

    public coinPaymentsDepositResponseModel getCoinPaymentsDepositResponse() {
        return coinPaymentsDepositResponse;
    }

    public void setCoinPaymentsDepositResponse(coinPaymentsDepositResponseModel coinPaymentsDepositResponse) {
        this.coinPaymentsDepositResponse = coinPaymentsDepositResponse;
    }

    public coinPaymentsWithdrawalResponseModel getCoinPaymentsWithdrawalResponse() {
        return coinPaymentsWithdrawalResponse;
    }

    public void setCoinPaymentsWithdrawalResponse(coinPaymentsWithdrawalResponseModel coinPaymentsWithdrawalResponse) {
        this.coinPaymentsWithdrawalResponse = coinPaymentsWithdrawalResponse;
    }

    public Double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Double receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public List<transactionStatusModel> getTransactionStatusList() {
        return transactionStatusList;
    }

    public void setTransactionStatusList(List<transactionStatusModel> transactionStatusList) {
        this.transactionStatusList = transactionStatusList;
    }

    public boolean isTransactionState() {
        return transactionState;
    }

    public void setTransactionState(boolean transactionState) {
        this.transactionState = transactionState;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getExchangeRateString() {
        return exchangeRateString;
    }

    public void setExchangeRateString(String exchangeRateString) {
        this.exchangeRateString = exchangeRateString;
    }
}
