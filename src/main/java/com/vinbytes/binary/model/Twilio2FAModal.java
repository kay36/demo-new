package com.vinbytes.binary.model;

import com.twilio.rest.verify.v2.service.Verification;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Twilio2FA")
public class Twilio2FAModal {


    @Id
    private String id;
    private String userId;
    private String twilioId;
    private String twoFAFor;
    private List<Verification> twilioOTPResponseModals;
    private List<TwilioStatusModal> twilioStatusModals;

    public Twilio2FAModal() {
    }

    public Twilio2FAModal(String id, String userId, String twilioId, String twoFAFor, List<Verification> twilioOTPResponseModals, List<TwilioStatusModal> twilioStatusModals) {
        this.id = id;
        this.userId = userId;
        this.twilioId = twilioId;
        this.twoFAFor = twoFAFor;
        this.twilioOTPResponseModals = twilioOTPResponseModals;
        this.twilioStatusModals = twilioStatusModals;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTwilioId() {
        return twilioId;
    }

    public void setTwilioId(String twilioId) {
        this.twilioId = twilioId;
    }

    public String getTwoFAFor() {
        return twoFAFor;
    }

    public void setTwoFAFor(String twoFAFor) {
        this.twoFAFor = twoFAFor;
    }

    public List<Verification> getTwilioOTPResponseModals() {
        return twilioOTPResponseModals;
    }

    public void setTwilioOTPResponseModals(List<Verification> twilioOTPResponseModals) {
        this.twilioOTPResponseModals = twilioOTPResponseModals;
    }

    public List<TwilioStatusModal> getTwilioStatusModals() {
        return twilioStatusModals;
    }

    public void setTwilioStatusModals(List<TwilioStatusModal> twilioStatusModals) {
        this.twilioStatusModals = twilioStatusModals;
    }
}
