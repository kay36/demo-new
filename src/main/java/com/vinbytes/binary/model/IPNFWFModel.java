package com.vinbytes.binary.model;


import org.springframework.data.annotation.Id;

public class IPNFWFModel {

    @Id
    private String id;
    private String address;
    private double amount;
    private long amounti;
    private String currency;
    private String coinPaymentsId;
    private String ipn_id;
    private String ipn_mode;
    private String ipn_type;
    private float ipn_version;
    private String merchant;
    private long status;
    private String status_text;
    private String txn_id;

    public IPNFWFModel() {
    }

    public IPNFWFModel(String id, String address, double amount, long amounti, String currency, String coinPaymentsId, String ipn_id, String ipn_mode, String ipn_type, float ipn_version, String merchant, long status, String status_text, String txn_id) {
        this.id = id;
        this.address = address;
        this.amount = amount;
        this.amounti = amounti;
        this.currency = currency;
        this.coinPaymentsId = coinPaymentsId;
        this.ipn_id = ipn_id;
        this.ipn_mode = ipn_mode;
        this.ipn_type = ipn_type;
        this.ipn_version = ipn_version;
        this.merchant = merchant;
        this.status = status;
        this.status_text = status_text;
        this.txn_id = txn_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getAmounti() {
        return amounti;
    }

    public void setAmounti(long amounti) {
        this.amounti = amounti;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCoinPaymentsId() {
        return coinPaymentsId;
    }

    public void setCoinPaymentsId(String coinPaymentsId) {
        this.coinPaymentsId = coinPaymentsId;
    }

    public String getIpn_id() {
        return ipn_id;
    }

    public void setIpn_id(String ipn_id) {
        this.ipn_id = ipn_id;
    }

    public String getIpn_mode() {
        return ipn_mode;
    }

    public void setIpn_mode(String ipn_mode) {
        this.ipn_mode = ipn_mode;
    }

    public String getIpn_type() {
        return ipn_type;
    }

    public void setIpn_type(String ipn_type) {
        this.ipn_type = ipn_type;
    }

    public float getIpn_version() {
        return ipn_version;
    }

    public void setIpn_version(float ipn_version) {
        this.ipn_version = ipn_version;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }

    public String getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }
}
